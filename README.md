# mvi-sp

Zadání semestrální práce:

Eukaryotní genomy se dají rozdělit podle různých pravidel na různé typy sekvencí. My si ho rozdělíme na 3 typy - introny a exony, které tvoří geny a na intergenové sekvence, které leží mimo geny. Intergenové sekvence a introny mají společnou vlastnost a to že jsou nekódující (nenesou informaci z které se poté skládá protein). Tyto typy sekvencí budu v diplomové práci klasifikovat z nukleotidové sekvence pomocí transformeru. Nejdřív je data ale třeba ztokenizovat, tj. převést string na vektor čísel. Právě problematika výběru správného tokenizéru (velikost slovníku, na jakých datech byl trénovaný, typ tokenizačního algoritmu) je předmětem práce. Zkouším dva různé typy algoritmu BPE (byte pair encoding) a to CharBPE a ByteLevelBPE. Připravená ztokenizovaná data se snažím klasifikovat za pomocí jednodušších machine learning klasifikátorů a podle úspěšnosti klasifikace vyberu tokenizér, kterým byla data zpracovaná. Ten pak použiji na preprocessing všech dat pro transformer v budoucnosti. Cílem celé práce je být schopen predikovat geny/porozumět jazyku DNA neznámého organismu za pomoci natrénovaného transformeru.
Zkouším 3 různé klasifikátory, ladím na nich parametry a dle vybraný model trénuji a na testovacích datech zobrazuji confusion matrix pro zmíněné 3 typy sekvencí, ROC křivku a další statistiky (precision, recall, f1 score).

Pokyny k dotažení potřebných genomů v podobě .fna a doplňkových .gff souborů na adresách:
- .fna soubory obsahují nukleotidové sekvence chromosomů a mitochondriální DNA v podobě multiplefasta souboru. Já pracuji pouze s chromosomy
- .gff soubory obsahují informace o koordinátách jednotlivých typů sekvencí v daných chromosomech (např. exon č.X se vysktuje v chromosomu č.ABC v intervalu 20 až 2300 písmenko)
- data_sample je soubor který ukazuje výslednou podobu po přípravě souborů pro další práci (dogenerování intronů pomocí AGAT tools a intronů pomocí jednoduchého skriptu)

Mus musculus	https://www.ncbi.nlm.nih.gov/genome/52?genome_assembly_id=992563

Felis catus	https://www.ncbi.nlm.nih.gov/genome/78?genome_assembly_id=356698

Rattus norvegicus	https://www.ncbi.nlm.nih.gov/genome/73?genome_assembly_id=1535500

Bos taurus	https://www.ncbi.nlm.nih.gov/genome/82?genome_assembly_id=371813

Sus scrofa	https://www.ncbi.nlm.nih.gov/genome/84?genome_assembly_id=317145

Ovis aries	https://www.ncbi.nlm.nih.gov/genome/83?genome_assembly_id=1549124

Canis lupus familiaris	https://www.ncbi.nlm.nih.gov/genome/85?genome_assembly_id=984575

Equus caballus	https://www.ncbi.nlm.nih.gov/genome/145?genome_assembly_id=358900

Ornitohoryhynchus anatinus	https://www.ncbi.nlm.nih.gov/genome/110?genome_assembly_id=1567774

Monodelphis domestica	https://www.ncbi.nlm.nih.gov/genome/220?genome_assembly_id=28569

Pokyny ke spuštění:
- Ve složce generovani_dat jsou jupyter notebooky
- Ze vzorového filu data_sample o složku výš je potřeba udělat raw formát za pomoci notebooku CreateRawFormatForTokenizers
- z raw souboru se generují tokenizéry za pomocí CharBPE a ByteLevelBPE notebooků - vytvoří se soubory merges.txt a vocab.json
- data pro trénování a testování se tvoří z data_sample pomocí notebooku TrainingSamplePreparation
- samotné trénování modelů a výběr tokenizérů je provedeno v notebooku ML_classification o složku výše 
